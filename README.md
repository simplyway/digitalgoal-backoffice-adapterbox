#EC Terminal
### ZVT Install

Download Dot.NET Core
https://dotnet.microsoft.com/en-us/download/dotnet/3.1
>Wichtig, es muss die Version 3.1 sein

* sudo mkdir /usr/share/dotnet
* sudo tar zxf dotnet-sdk-3.1.XXX-linux-arm.tar.gz -C /usr/share/dotnet
* export DOTNET_ROOT=/usr/share/dotnet
* export PATH=$PATH:/usr/share/dotnet

#### Test
dotnet --version

---