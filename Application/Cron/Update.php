<?php
/**
 * Created by DigitalGoal Digitalagentur
 */


require_once __DIR__ . '/../Bootstrap.php';

$job = new \Model\Job([
    'job_id' => 0,
    'job_device_serial' => '',
    'job_device_name' => '',
    'job_type' => '',
    'job_data' => '{}',
    'job_response' => '}',
    'job_status' => '',
    'job_start' => '',
    'job_end' => '',
    'job_updated' => '',
    'job_created' => '',
]);

\Job\Types\Update::run($job);