<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

require_once __DIR__ . '/../Bootstrap.php';

$printers = \Service\Printer::getAllSystemPrinters();

foreach ($printers as $printer) {

    if (!isset($printer['name']) || empty($printer['name'])) {
        continue;
    }

    $enroll = \Service\Printer::printerEnroll($printer['name']);

}