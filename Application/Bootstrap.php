<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');
define('APPLICATION_PATH', __DIR__);
define('DS', DIRECTORY_SEPARATOR);

header("Content-Type: text/html; charset=utf-8");
date_default_timezone_set('Europe/Berlin');

require_once APPLICATION_PATH . '/Lib/Autoload.php';
spl_autoload_register(array('Autoload', 'load'));

require_once APPLICATION_PATH . '/../vendor/autoload.php';

if (
    !file_exists(__DIR__ . '/Config/development.ini') &&
    !file_exists(__DIR__ . '/Config/production.ini')
) {
    print 'Config file not exsits - Create production.ini it in Config folder';
    die;
}