#!/bin/bash
apt update
apt install git
apt install cups
sudo usermod -aG lpadmin pi
apt install php php-gd php-intl php-curl php-mbstring php-xml
apt install composer
apt install supervisor
cp Applcation/Tools/Supervisor/run.conf /etc/supervisor/conf.d/run.conf