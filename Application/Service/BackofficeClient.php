<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Service;

use Lib\Config;

class BackofficeClient
{

    const REQUEST_METHOD_GET = 'GET';
    const REQUEST_METHOD_PUT = 'PUT';
    const REQUEST_METHOD_POST = 'POST';
    const REQUEST_METHOD_DELETE = 'DELETE';

    const ENDPOINT_JOB = 'job';
    const ENDPOINT_PRINTER = 'printer';

    /**
     * @param $endpoint
     * @param array $params
     * @return array
     */
    public static function getRequest($endpoint, $params = [])
    {
        return static::request($endpoint, static::REQUEST_METHOD_GET, $params);
    }


    /**
     * @param $endpoint
     * @param array $params
     * @return array
     */
    public static function postRequest($endpoint, $params = [])
    {
        return static::request($endpoint, static::REQUEST_METHOD_POST, $params);
    }

    /**
     * @param $endpoint
     * @param array $params
     * @return array
     */
    public static function putRequest($endpoint, $params = [])
    {
        return static::request($endpoint, static::REQUEST_METHOD_PUT, $params);
    }

    /**
     * @param $endpoint
     * @param array $params
     * @return array
     */
    public static function deleteRequest($endpoint, $params = [])
    {
        return static::request($endpoint, static::REQUEST_METHOD_DELETE, $params);
    }


    /**
     * @param $endpoint
     * @param $method
     * @param array $params
     * @return array
     */
    private static function request($endpoint, $method, $params = [])
    {
        $params = http_build_query($params);

        $response = file_get_contents(
            Config::getConfig()['api'] . $endpoint . '?' . $params,
            false,
            static::getContext($method)
        );

        $response = json_decode($response, true);

        return $response;
    }

    /**
     * @param string $method
     * @return resource
     */
    private static function getContext($method = self::REQUEST_METHOD_GET)
    {
        $opts = [
            'http' => [
                'method' => $method,
                'header' => [
                    'Authorization' => 'Authorization: ' . Config::getConfig()['token']
                ],
            ]
        ];
        return stream_context_create($opts);
    }

}