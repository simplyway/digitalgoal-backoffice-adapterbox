<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Service;

use Lib\Config;

class Printer
{

    public static function getAllSystemPrinters()
    {

        $command = shell_exec("lpstat -d -p");
        $res = explode("\n", $command);
        $i = 0;
        foreach ($res as $r) {

            $active = 0;
            if (strpos($r, "printer") !== FALSE) {
                $r = str_replace("printer ", "", $r);
                if (strpos($r, "is idle") !== FALSE)
                    $active = 1;

                $r = explode(" ", $r);

                $printers[$i]['name'] = $r[0];
                $printers[$i]['active'] = $active;
                $i++;
            }
        }
        return $printers;
    }

    public static function printerEnroll($name)
    {

        $config = Config::getConfig();

        return BackofficeClient::postRequest(BackofficeClient::ENDPOINT_PRINTER, [
            'printer_serial' => $config['serial'],
            'printer_name' => $name,
            'printer_default_bon' => 0,
            'printer_default_invoice' => 0,
            'printer_default_shipment' => 0
        ]);

    }

}