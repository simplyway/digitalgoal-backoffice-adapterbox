<?php
/**
 * Created by DigitalGoal Digitalagentur
 */


require_once __DIR__ . '/Bootstrap.php';



while($up = true) {

    $jobs = \Service\BackofficeClient::getRequest(\Service\BackofficeClient::ENDPOINT_JOB);

    if (
        !empty($jobs) ||
        isset($jobs['items']) &&
        count($jobs['items']) > 0
    ) {

        foreach ($jobs['items'] as $job) {

            $runner = \Job\Job::run(
                (new \Model\Job($job))
            );

        }

    }

    sleep(5);

}
