<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Job\Types;

use Model\Job;

class EcLog
{
    const ZVT_LOG_FILE = 'ZVT_Status_1.xml';

    public static function run(Job $job)
    {

        $file = 'ZVT-' . date('Y-m-d') . '.LOG';
        if (!file_exists(__DIR__ . '/../../' . $file)) {
            return '';
        }

        return file_get_contents(__DIR__ . '/../../' . $file);

    }
}