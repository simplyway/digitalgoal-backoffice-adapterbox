<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Job\Types;

use Model\Job;

class EcTerminal
{
    const ZVT_STATUS_FILE = 'ZVT_Status_1.xml';
    const ZVT_ERGEBNIS_FILE = 'ZVT_Ergebnis_1.xml';

    public static function run(Job $job)
    {
        $jobData = $job->getJobData();

        if (
            !isset($jobData['ip']) ||
            !isset($jobData['port']) ||
            !isset($jobData['amount'])
        ) {
            return false;
        }

        $command = [];
        $command[] = 'sudo ' . __DIR__ . '/../../Tools/EasyZVT_Core';
        $command[] = 'KasseNr=1';
        $command[] = 'COM=LAN';
        $command[] = 'IP=' . $jobData['ip'];
        $command[] = 'PORT=' . $jobData['port'];
        $command[] = 'Betrag=' . $jobData['amount'];
        $command[] = 'Ausgabepfad=' . __DIR__ . '/../../Data/';

        $result = shell_exec(implode(' ', $command));

        $xml = file_get_contents(__DIR__ . '/../../Data/' . static::ZVT_ERGEBNIS_FILE);
        $xml = simplexml_load_string($xml);
        $xml = json_encode($xml);
        $xml = json_decode($xml,true);

        return json_encode([
            'console' => $result,
            'xml' => $xml
        ]);

    }
}