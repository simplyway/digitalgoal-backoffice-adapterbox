<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Job\Types;

use model\devicejob;
use Model\Job;
use service\device\bonItem;

class Bon
{
    /**
     * @param Job $job
     * @return bool
     * @throws \Gumlet\ImageResizeException
     */
    public static function run(Job $job)
    {

        $jobData = $job->getJobData();

        $items = [];
        $taxes = [];
        $sumInvoicePositions = 0;

        foreach ($jobData['invoicepositions'] as $invoicePosition) {

            if (!isset($taxes[$invoicePosition['invoiceposition_product_tax']])) {
                $taxes[$invoicePosition['invoiceposition_product_tax']] = 0;
            }

            $taxes[$invoicePosition['invoiceposition_product_tax']] += $invoicePosition['invoiceposition_product_tax_amount'];
            $items[] = new \Job\Types\Bon\BonItem($invoicePosition['product_name'], number_format($invoicePosition['invoiceposition_product_price_netto'], 2, ',', '.'));

            $sumInvoicePositions += $invoicePosition['invoiceposition_product_price_netto'];

        }

        $subtotal = new \Job\Types\Bon\BonItem('Summe', number_format($sumInvoicePositions, 2, ',', '.'));
        $date = date('d.m.Y', strtotime($jobData['invoice_date']));

        $logo = \Mike42\Escpos\EscposImage::load(static::logo($jobData), true);

        $tmpfname = 'printout';
        $connector = new \Mike42\Escpos\PrintConnectors\FilePrintConnector($tmpfname);
        $profile = \Mike42\Escpos\CapabilityProfile::load("default");
        $printer = new \Mike42\Escpos\Printer($connector, $profile);

        $printer->setJustification(\Mike42\Escpos\Printer::JUSTIFY_CENTER);
        $printer->setEmphasis(true);
        $printer->bitImage($logo);
        $printer->setEmphasis(false);

        $printer->setTextSize(1, 1);
        $printer->text(static::convertTextString($jobData['mandator_name']) . "\n");
        $printer->selectPrintMode();
        $printer->text(static::convertTextString($jobData['mandator_street']) . "\n");
        $printer->text(static::convertTextString($jobData['mandator_zipcode'] . " " . $jobData['mandator_city']) . "\n");
        $printer->feed();

        $printer->setEmphasis(true);
        $printer->text("RECHNUNG\n");
        $printer->text($jobData['invoice_number'] . "\n");
        $printer->setEmphasis(false);
        $printer->feed();

        $printer->setJustification(\Mike42\Escpos\Printer::JUSTIFY_LEFT);
        foreach ($items as $item) {
            $printer->text($item);
        }
        $printer->setEmphasis(true);
        $printer->text($subtotal);
        $printer->setEmphasis(false);
        $printer->feed();

        \Job\Types\Bon\BonItem::resetRightCols();
        $items = [];

        foreach ($taxes as $taxPercent => $taxAmount) {
            $items[] = new \Job\Types\Bon\BonItem(
                'Steuer ' . $taxPercent . '%',
                static::convertDecimalString($taxAmount)
            );
        }
        foreach ($items as $item) {
            $printer->text($item);
        }

        $printer->text(new \Job\Types\Bon\BonItem(
            'Gesamt EURO',
            static::convertDecimalString($jobData['invoice_amount'])
        ));
        $printer->selectPrintMode();

        $printer->feed(2);
        $printer->setJustification(\Mike42\Escpos\Printer::JUSTIFY_CENTER);
        $printer->text("Vielen Dank\n");
        $printer->text(static::convertTextString($jobData['mandator_name']) . "\n");
        $printer->feed(2);
        $printer->text("Kontakt\n");
        $printer->text(static::convertTextString($jobData['mandator_phone']) . "\n");
        $printer->text(static::convertTextString($jobData['mandator_email']) . "\n");
        $printer->feed(2);
        $printer->text('Rechnungsdatum ' . $date . "\n");
        $printer->text('Erstellt ' . date('d.m.Y H:i:s', strtotime($jobData['invoice_updated'])) . "\n");
        $printer->feed(3);

        /* Cut the receipt and open the cash drawer */
        $printer->cut();
        $printer->pulse();
        $printer->close();

        $cmd = sprintf(
            "lp -d " . $job->getJobDeviceName() . " %s ",
            escapeshellarg($tmpfname)
        );

        exec($cmd, $retArr, $retVal);

        /* cleanup */

        @unlink(__DIR__ . $pathMandatorLogo);
        @unlink(__DIR__ . $pathBonLogo);
        @unlink($tmpfname);

        return true;

    }

    public static function convertTextString($string)
    {

        $search = array("Ä", "Ö", "Ü", "ä", "ö", "ü", "ß", "´");
        $replace = array("Ae", "Oe", "Ue", "ae", "oe", "ue", "ss", "");
        return str_replace($search, $replace, $string);

    }

    public static function convertDecimalString($string)
    {
        return number_format($string, 2, ',', '.');
    }

    public static function logo($jobData)
    {

        $pathBonLogo = '/../../Data/logo_bon.png';

        $image = \Gumlet\ImageResize::createFromString(base64_decode($jobData['mandator_logo']));
        $image->resizeToWidth(385);
        $image->save(__DIR__ . $pathBonLogo);

        return $pathBonLogo;

    }

}