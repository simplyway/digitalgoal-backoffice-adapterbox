<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Job\Types;

use Model\Job;

class Printerlog
{
    public static function run(Job $job)
    {
        return shell_exec('lpstat -W all -o ' . $job->getJobDeviceName());
    }
}