<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Job\Types;

use Model\Job;

class DocumentPrint
{

    const CONTENT_TYPE_PDF = 'pdf';

    public static function run(Job $job)
    {

        if (!isset($job->getJobData()['content_type'])) {
            return false;
        }

        switch ($job->getJobData()['content_type']) {
            case static::CONTENT_TYPE_PDF:

                $tmpFile = __DIR__ . '/../../Data/tmpPdf.pdf';
                file_put_contents($tmpFile, base64_decode($job->getJobData()['content']));
                shell_exec('lpr -P ' . $job->getJobDeviceName() . ' ' . $tmpFile);
                unlink($tmpFile);
                break;
        }

    }
}