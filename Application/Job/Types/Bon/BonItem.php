<?php

namespace Job\Types\Bon;

class BonItem
{
    private $name;
    private $price;
    private static $rightCols = 0;
    private static $cols = 32;

    /**
     * BonItem constructor.
     * @param string $name
     * @param string $price
     */
    public function __construct($name = '', $price = '')
    {
        $this->name = $name;
        $this->price = $price;
        if ($this::$rightCols < strlen($price)) {
            $this::$rightCols = strlen($price);
        }
    }

    public static function resetRightCols()
    {
        self::$rightCols = 0;
    }

    public static function setCols($cols)
    {
        self::$cols = $cols;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $stringLeft = $this->cutText($this->name, $this::$cols - $this::$rightCols);
        $stringRight = $this->cutText($this->price, $this::$rightCols);

        $left = str_pad($stringLeft, $this::$cols - $this::$rightCols);
        $right = str_pad($stringRight, $this::$rightCols, ' ', STR_PAD_LEFT);

        return "$left$right\n";
    }

    /**
     * @param $string
     * @param $length
     * @return string
     */
    public function cutText($string, $length)
    {
        return strlen($string) > $length ? substr($string, 0, $length - 3) . "..." : $string;
    }
}