<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Job;

use Job\Types\Bon;
use Job\Types\DocumentPrint;
use Job\Types\EcLog;
use Job\Types\EcTerminal;
use Job\Types\Printerlog;
use Job\Types\Update;
use Service\BackofficeClient;

class Job
{

    const JOB_TYPE_PRINT = 'print';
    const JOB_TYPE_PRINTERLOG = 'printerlog';
    const JOB_TYPE_BON = 'bon';
    const JOB_TYPE_UPDATE = 'update';
    const JOB_TYPE_ECTERMINAL = 'ecterminal';
    const JOB_TYPE_ECLOG = 'eclog';

    public static function run(\Model\Job $job)
    {

        $result = false;
        $job->setJobStart(date('Y-m-d H:i:s'));
        $job->setJobStatus('pulled');
        static::changeState($job);

        switch ($job->getJobType()) {
            case static::JOB_TYPE_UPDATE:
                $result = Update::run($job);
                break;
            case static::JOB_TYPE_PRINT:
                $result = DocumentPrint::run($job);
                break;
            case static::JOB_TYPE_BON:
                $result = Bon::run($job);
                break;
            case static::JOB_TYPE_PRINTERLOG:
                $result = Printerlog::run($job);
                break;
            case static::JOB_TYPE_ECTERMINAL:
                $result = EcTerminal::run($job);
                break;
            case static::JOB_TYPE_ECLOG:
                $result = EcLog::run($job);
                break;
        }

        if ($result === false) {
            $job->setJobStatus('error');
        } else {
            $job->setJobEnd(date('Y-m-d H:i:s'));
            $job->setJobStatus('done');
            $job->setJobResponse($result);
        }

        static::changeState($job);

        return $result;

    }

    public static function changeState(\Model\Job $job)
    {
        BackofficeClient::putRequest(
            BackofficeClient::ENDPOINT_JOB . '/' . $job->getJobId(),
            [
                'job_start' => $job->getJobStart(),
                'job_end' => $job->getJobEnd(),
                'job_status' => $job->getJobStatus(),
                'job_response' => $job->getJobResponse()
            ]
        );
    }

}