<?php

namespace Model;

class Bon
{

    protected $mandatorLogo = '';
    protected $mandatorName = '';
    protected $mandatorOwner = '';
    protected $mandatorStreet = '';
    protected $mandatorZipcode = '';
    protected $mandatorCity = '';
    protected $mandatorCountry = '';
    protected $mandatorPhone = '';
    protected $mandatorFax = '';
    protected $mandatorEmail = '';

    protected $invoiceNumber = '';
    protected $invoiceDate = '';
    protected $invoicePositions = [];

    protected $invoiceSumNetto = 0;
    protected $invoiceTaxes = [];
    protected $invoiceBonPositions = [];

    public function __construct($raw)
    {

        $this->setMandatorName($raw['mandator_logo'])
            ->setMandatorName($raw['mandator_name'])
            ->setMandatorOwner($raw['mandator_name'])
            ->setMandatorStreet($raw['mandator_name'])
            ->setMandatorZipcode($raw['mandator_name'])
            ->setMandatorCity($raw['mandator_name'])
            ->setMandatorCountry($raw['mandator_name'])
            ->setMandatorPhone($raw['mandator_name'])
            ->setMandatorFax($raw['mandator_name'])
            ->setMandatorEmail($raw['mandator_name']);

        $this->setInvoiceNumber($raw['invoice_number'])
            ->setInvoiceDate($raw['invoice_date'])
            ->setInvoicePositions($raw['invoicepositions']);

        foreach ($this->getInvoicePositions() as $invoicePosition) {

            if (!isset($this->taxes[$invoicePosition['invoiceposition_product_tax']])) {
                $this->invoiceTaxes[$invoicePosition['invoiceposition_product_tax']] = 0;
            }

            $this->invoiceTaxes[$invoicePosition['invoiceposition_product_tax']] += $invoicePosition['invoiceposition_product_tax_amount'];
            $this->invoiceBonPositions[] = [
                'name' => $invoicePosition['product_name'],
                'price' => number_format($invoicePosition['invoiceposition_product_price_netto'], 2, ',', '.')
            ];

            $this->invoiceSumNetto += $invoicePosition['invoiceposition_product_price_netto'];

        }


    }

    /**
     * @return string
     */
    public function getMandatorLogo(): string
    {
        return $this->mandatorLogo;
    }

    /**
     * @param string $mandatorLogo
     * @return bon
     */
    public function setMandatorLogo(string $mandatorLogo): bon
    {
        $this->mandatorLogo = $mandatorLogo;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorName(): string
    {
        return $this->mandatorName;
    }

    /**
     * @param string $mandatorName
     * @return bon
     */
    public function setMandatorName(string $mandatorName): bon
    {
        $this->mandatorName = $mandatorName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorOwner(): string
    {
        return $this->mandatorOwner;
    }

    /**
     * @param string $mandatorOwner
     * @return bon
     */
    public function setMandatorOwner(string $mandatorOwner): bon
    {
        $this->mandatorOwner = $mandatorOwner;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorStreet(): string
    {
        return $this->mandatorStreet;
    }

    /**
     * @param string $mandatorStreet
     * @return bon
     */
    public function setMandatorStreet(string $mandatorStreet): bon
    {
        $this->mandatorStreet = $mandatorStreet;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorZipcode(): string
    {
        return $this->mandatorZipcode;
    }

    /**
     * @param string $mandatorZipcode
     * @return bon
     */
    public function setMandatorZipcode(string $mandatorZipcode): bon
    {
        $this->mandatorZipcode = $mandatorZipcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorCity(): string
    {
        return $this->mandatorCity;
    }

    /**
     * @param string $mandatorCity
     * @return bon
     */
    public function setMandatorCity(string $mandatorCity): bon
    {
        $this->mandatorCity = $mandatorCity;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorCountry(): string
    {
        return $this->mandatorCountry;
    }

    /**
     * @param string $mandatorCountry
     * @return bon
     */
    public function setMandatorCountry(string $mandatorCountry): bon
    {
        $this->mandatorCountry = $mandatorCountry;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorPhone(): string
    {
        return $this->mandatorPhone;
    }

    /**
     * @param string $mandatorPhone
     * @return bon
     */
    public function setMandatorPhone(string $mandatorPhone): bon
    {
        $this->mandatorPhone = $mandatorPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorFax(): string
    {
        return $this->mandatorFax;
    }

    /**
     * @param string $mandatorFax
     * @return bon
     */
    public function setMandatorFax(string $mandatorFax): bon
    {
        $this->mandatorFax = $mandatorFax;
        return $this;
    }

    /**
     * @return string
     */
    public function getMandatorEmail(): string
    {
        return $this->mandatorEmail;
    }

    /**
     * @param string $mandatorEmail
     * @return bon
     */
    public function setMandatorEmail(string $mandatorEmail): bon
    {
        $this->mandatorEmail = $mandatorEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber(): string
    {
        return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return bon
     */
    public function setInvoiceNumber(string $invoiceNumber): bon
    {
        $this->invoiceNumber = $invoiceNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceDate(): string
    {
        return $this->invoiceDate;
    }

    /**
     * @param string $invoiceDate
     * @return bon
     */
    public function setInvoiceDate(string $invoiceDate): bon
    {
        $this->invoiceDate = $invoiceDate;
        return $this;
    }

    /**
     * @return array
     */
    public function getInvoicePositions(): array
    {
        return $this->invoicePositions;
    }

    /**
     * @param array $invoicePositions
     * @return bon
     */
    public function setInvoicePositions(array $invoicePositions): bon
    {
        $this->invoicePositions = $invoicePositions;
        return $this;
    }

    /**
     * @return int|mixed
     */
    public function getInvoiceSumNetto()
    {
        return $this->invoiceSumNetto;
    }

    /**
     * @param int|mixed $invoiceSumNetto
     * @return bon
     */
    public function setInvoiceSumNetto($invoiceSumNetto)
    {
        $this->invoiceSumNetto = $invoiceSumNetto;
        return $this;
    }

    /**
     * @return array
     */
    public function getInvoiceTaxes(): array
    {
        return $this->invoiceTaxes;
    }

    /**
     * @param array $invoiceTaxes
     * @return bon
     */
    public function setInvoiceTaxes(array $invoiceTaxes): bon
    {
        $this->invoiceTaxes = $invoiceTaxes;
        return $this;
    }

    /**
     * @return array
     */
    public function getInvoiceBonPositions(): array
    {
        return $this->invoiceBonPositions;
    }

    /**
     * @param array $invoiceBonPositions
     * @return bon
     */
    public function setInvoiceBonPositions(array $invoiceBonPositions): bon
    {
        $this->invoiceBonPositions = $invoiceBonPositions;
        return $this;
    }


}