<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Model;

class Printer
{

    /**
     * @var int
     */
    protected $printerId;

    /**
     * @var string
     */
    protected $printerSerial;

    /**
     * @var string
     */
    protected $printerType;

    /**
     * @var string
     */
    protected $printerLocation;

    /**
     * @var string
     */
    protected $printerTitle;

    /**
     * @var string
     */
    protected $printerName;

    /**
     * @var string
     */
    protected $printerCommand;

    /**
     * @var string
     */
    protected $printerCreated;

    /**
     * @var string
     */
    protected $printerUpdated;

    public function __construct()
    {

    }

    /**
     * @return int
     */
    public function getPrinterId(): int
    {
        return $this->printerId;
    }

    /**
     * @param int $printerId
     * @return Printer
     */
    public function setPrinterId(int $printerId): Printer
    {
        $this->printerId = $printerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterSerial(): string
    {
        return $this->printerSerial;
    }

    /**
     * @param string $printerSerial
     * @return Printer
     */
    public function setPrinterSerial(string $printerSerial): Printer
    {
        $this->printerSerial = $printerSerial;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterType(): string
    {
        return $this->printerType;
    }

    /**
     * @param string $printerType
     * @return Printer
     */
    public function setPrinterType(string $printerType): Printer
    {
        $this->printerType = $printerType;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterLocation(): string
    {
        return $this->printerLocation;
    }

    /**
     * @param string $printerLocation
     * @return Printer
     */
    public function setPrinterLocation(string $printerLocation): Printer
    {
        $this->printerLocation = $printerLocation;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterTitle(): string
    {
        return $this->printerTitle;
    }

    /**
     * @param string $printerTitle
     * @return Printer
     */
    public function setPrinterTitle(string $printerTitle): Printer
    {
        $this->printerTitle = $printerTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterName(): string
    {
        return $this->printerName;
    }

    /**
     * @param string $printerName
     * @return Printer
     */
    public function setPrinterName(string $printerName): Printer
    {
        $this->printerName = $printerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterCommand(): string
    {
        return $this->printerCommand;
    }

    /**
     * @param string $printerCommand
     * @return Printer
     */
    public function setPrinterCommand(string $printerCommand): Printer
    {
        $this->printerCommand = $printerCommand;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterCreated(): string
    {
        return $this->printerCreated;
    }

    /**
     * @param string $printerCreated
     * @return Printer
     */
    public function setPrinterCreated(string $printerCreated): Printer
    {
        $this->printerCreated = $printerCreated;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterUpdated(): string
    {
        return $this->printerUpdated;
    }

    /**
     * @param string $printerUpdated
     * @return Printer
     */
    public function setPrinterUpdated(string $printerUpdated): Printer
    {
        $this->printerUpdated = $printerUpdated;
        return $this;
    }

}