<?php
/**
 * Created by DigitalGoal Digitalagentur
 */

namespace Model;

class Job
{

    /**
     * @var int
     */
    protected $jobId;

    /**
     * @var string
     */
    protected $jobDeviceSerial;

    /**
     * @var string
     */
    protected $jobDeviceName;

    /**
     * @var string
     */
    protected $jobType;

    /**
     * @var string|array
     */
    protected $jobData;

    /**
     * @var string
     */
    protected $jobResponse;

    /**
     * @var string
     */
    protected $jobStatus;

    /**
     * @var string
     */
    protected $jobStart;

    /**
     * @var string
     */
    protected $jobEnd;

    /**
     * @var string
     */
    protected $jobUpdated;

    /**
     * @var string
     */
    protected $jobCreated;

    /**
     * Job constructor.
     * @param array $raw
     */
    public function __construct(array $raw)
    {

        $this
            ->setJobId($raw['job_id'])
            ->setJobDeviceSerial($raw['job_device_serial'])
            ->setJobDeviceName($raw['job_device_name'])
            ->setJobType($raw['job_type'])
            ->setJobData(json_decode($raw['job_data'], true))
            ->setJobResponse($raw['job_response'])
            ->setJobStatus($raw['job_status'])
            ->setJobStart($raw['job_start'])
            ->setJobEnd($raw['job_end'])
            ->setJobEnd($raw['job_updated'])
            ->setJobEnd($raw['job_created']);

    }

    /**
     * @return int
     */
    public function getJobId(): int
    {
        return $this->jobId;
    }

    /**
     * @param int $jobId
     * @return Job
     */
    public function setJobId(int $jobId): Job
    {
        $this->jobId = $jobId;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobDeviceSerial(): string
    {
        return $this->jobDeviceSerial;
    }

    /**
     * @param string $jobDeviceSerial
     * @return Job
     */
    public function setJobDeviceSerial(string $jobDeviceSerial): Job
    {
        $this->jobDeviceSerial = $jobDeviceSerial;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobDeviceName(): string
    {
        return $this->jobDeviceName;
    }

    /**
     * @param string $jobDeviceName
     * @return Job
     */
    public function setJobDeviceName(string $jobDeviceName): Job
    {
        $this->jobDeviceName = $jobDeviceName;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobType(): string
    {
        return $this->jobType;
    }

    /**
     * @param string $jobType
     * @return Job
     */
    public function setJobType(string $jobType): Job
    {
        $this->jobType = $jobType;
        return $this;
    }

    /**
     * @return array
     */
    public function getJobData()
    {
        return $this->jobData;
    }

    /**
     * @param string|string $jobData
     * @return Job
     */
    public function setJobData($jobData): Job
    {
        $this->jobData = $jobData;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobResponse()
    {
        return $this->jobResponse;
    }

    /**
     * @param string $jobResponse
     * @return Job
     */
    public function setJobResponse($jobResponse): Job
    {
        $this->jobResponse = $jobResponse;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobStatus(): string
    {
        return $this->jobStatus;
    }

    /**
     * @param string $jobStatus
     * @return Job
     */
    public function setJobStatus(string $jobStatus): Job
    {
        $this->jobStatus = $jobStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobStart(): string
    {
        return $this->jobStart;
    }

    /**
     * @param string $jobStart
     * @return Job
     */
    public function setJobStart(?string $jobStart): Job
    {
        $this->jobStart = $jobStart;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobEnd(): string
    {
        return $this->jobEnd;
    }

    /**
     * @param string $jobEnd
     * @return Job
     */
    public function setJobEnd(?string $jobEnd): Job
    {
        $this->jobEnd = $jobEnd;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobUpdated(): string
    {
        return $this->jobUpdated;
    }

    /**
     * @param string $jobUpdated
     * @return Job
     */
    public function setJobUpdated(?string $jobUpdated): Job
    {
        $this->jobUpdated = $jobUpdated;
        return $this;
    }

    /**
     * @return string
     */
    public function getJobCreated(): string
    {
        return $this->jobCreated;
    }

    /**
     * @param string $jobCreated
     * @return Job
     */
    public function setJobCreated(?string $jobCreated): Job
    {
        $this->jobCreated = $jobCreated;
        return $this;
    }

}