<?php

class Autoload {

    public static function load($class) {

        $directorys = explode('\\', $class);
        $path = APPLICATION_PATH . '/' . implode('/', $directorys) . '.php';

        if (file_exists($path)) {

            require_once $path;

        } else {

            return false;

        }
    }

}