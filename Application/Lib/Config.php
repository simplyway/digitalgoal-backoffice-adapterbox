<?php

/**
 * Class Config
 */

namespace Lib;

class Config
{

    /**
     * @var String
     */
    const CONFIG_PATH = '/../Config/';

    /**
     * @param String $enviroment
     *
     * @return array
     */
    public static function getConfig()
    {

        if (file_exists(__DIR__ . self::CONFIG_PATH . 'development.ini')) {
            return parse_ini_file(__DIR__ . self::CONFIG_PATH . 'development.ini');
        }

        return parse_ini_file(__DIR__ . self::CONFIG_PATH . 'production.ini');

    }

    public static function getDeviceName()
    {

        $config = static::getConfig();
        return $config['devicename'];

    }

}
